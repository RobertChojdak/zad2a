package com.robert.zadanie2;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.TvSeries;
import model.Director;


public class TvSeriesAction {
	
	List <TvSeries> tvseries = new ArrayList();
	private String addSQL = "INSERT INTO TVSERIES (NAME, DIRECTOR_ID) VALUES (?, ?)";
	private String selectALLSQL = "SELECT * FROM TVSERIES";
	private String deleteAllSQL = "DELETE FROM TVSERIES";
	private String findIdSQL = "SELECT ID FROM TVSERIES WHERE NAME = ?";
	
	public void add(DBConnect connection, TvSeries tvserie, String directorName) throws SQLException {
		DirectorAction director = new DirectorAction();
		int id = director.getId(connection, directorName);
		PreparedStatement addActorQuery = connection.getConnection().prepareStatement(addSQL);
		addActorQuery.setString(1, tvserie.getName());
		addActorQuery.setInt(2, id);
		addActorQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			TvSeries tvserie = new TvSeries();
			tvserie. setId(selectAllQuery.getInt("ID"));
			tvserie.setName(selectAllQuery.getString("NAME"));
			tvseries.add(tvserie);
		}
		selectAllQuery.beforeFirst();
		return tvseries;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteTvSeriesQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteTvSeriesQuery.executeUpdate();
	}
	
	public int getId(DBConnect connection, String tvSeriesName) throws SQLException {
		int result = 0;
		PreparedStatement findIdQuery = connection.getConnection().prepareStatement(findIdSQL);
		findIdQuery.setString(1, tvSeriesName);
		ResultSet rs = findIdQuery.executeQuery();
		if(rs.next()) {
			result = rs.getInt(1);
		}
		return result;
	}
}