package com.robert.zadanie2;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import model.*;

public class Main {

	public static void main(String[] args) throws SQLException {
		
		List<Actor> actors = new ArrayList();
		//Date values
		LocalDate actorDate1 = LocalDate.of(1990, 11, 2);
		LocalDate directorDate1 = LocalDate.of(1990, 11, 2);
		LocalDate directorDate2 = LocalDate.of(1979, 9, 5);
		
		// Seasons
		Season season1 = new Season();
		season1.setSeasonNumber(1);
		season1.setYearOfRelease(1979);
		//System.out.println(season1.toString());
		
		//Episodes
		//Episode episode1 = new Episode("Początek", releaseDate1, 1, 234);
		//System.out.println(episode1.toString());
		
		//Directors
		Director director1 = new Director("Marek Koniarek", directorDate1, "Grown alone");
		Director director2 = new Director("Jurek Pazdan", directorDate2, "Gdedcfc");
		//System.out.println(director1.toString());
		
		//Actors
		Actor actor1 = new Actor("Tomasz Powwer", actorDate1, "vekcmel cde3ce3c");
		//System.out.println(actor1.toString());
		
		//Actor actor2 = new Actor();
		//actor2.setName("Kazik Magiros");
		//actor2.setDateOfBirth(actorDate2);
		//actor2.setBiography("Jeszcze nie ma");
		//System.out.println(actor2.toString());
		
		//Adding seasons to TvSerie;
		//tvSerie1.addSeasons(season1);
				
		//Adding episodes to season
		//season1.addEpisodes(episode1);
			
		//List<Season> seasons = tvSerie1.getSeasons();
		//for(Season season: seasons){
			//System.out.println(season);
		//}
		
		DBConnect connection = new DBConnect();
		ActorAction actorAction1 = new ActorAction();
		//actorAction1.addActor(connection, actor1);
		actors = actorAction1.selectAll(connection);
		for(Actor actor: actors) {
			System.out.println(actor.toString());
		}
		//actorAction1.deleteAll(connection);
		
		DirectorAction directorAction1 = new DirectorAction();
		directorAction1.add(connection, director1);
		directorAction1.add(connection, director2);
		//System.out.println(directorAction1.getId(connection, "Jurek Pazdan"));
		
		//TV Series
		TvSeries tvSerie1 = new TvSeries();
		tvSerie1.setName("Star Trek");
		
		TvSeriesAction tvaction = new TvSeriesAction();
		tvaction.add(connection, tvSerie1, "Jurek Pazdan");
		
		
	}
}

