package com.robert.zadanie2;

import java.util.*;
import model.Actor;
import java.sql.*;
import java.sql.Date;


public class ActorAction {
	
	List <Actor> actors = new ArrayList();
	private String addSQL = "INSERT INTO ACTOR (NAME, DATE_OF_BIRTH, BIOGRAPHY) VALUES (?, ?, ?)";
	private String selectALLSQL = "SELECT * FROM ACTOR";
	private String deleteAllSQL = "DELETE FROM ACTOR";
	private String findIdSQL = "SELECT ID FROM ACTOR WHERE NAME = ?";
		
	public void add(DBConnect connection, Actor actor) throws SQLException {
		PreparedStatement addActorQuery = connection.getConnection().prepareStatement(addSQL);
		addActorQuery.setString(1, actor.getName());
		addActorQuery.setDate(2, Date.valueOf(actor.getDateOfBirth()));
		addActorQuery.setString(3, actor.getBiography());
		addActorQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAll = statement.executeQuery(selectALLSQL);
		selectAll.beforeFirst();
		while(selectAll.next()) {
			Actor actor = new Actor();
			actor. setId(selectAll.getInt("ID"));
			actor.setName(selectAll.getString("NAME"));
			actor.setDateOfBirth(selectAll.getDate("DATE_OF_BIRTH").toLocalDate());
			actor.setBiography(selectAll.getString("BIOGRAPHY"));
			actors.add(actor);
		}
		selectAll.beforeFirst();
		return actors;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteActorsQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteActorsQuery.executeUpdate();
	}
	
	public int getId(DBConnect connection, String actorName) throws SQLException {
		int result = 0;
		PreparedStatement findIdQuery = connection.getConnection().prepareStatement(findIdSQL);
		findIdQuery.setString(1, actorName);
		ResultSet rs = findIdQuery.executeQuery();
		if(rs.next()) {
			result = rs.getInt(1);
		}
		return result;
	}
	
}