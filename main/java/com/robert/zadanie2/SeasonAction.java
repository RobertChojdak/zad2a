package com.robert.zadanie2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Season;

public class SeasonAction {
	
	List <Season> seasons = new ArrayList();
	private String addSQL = "INSERT INTO SEASON (SEASON_NUMBER INT, YEAR_OF_RELEASE INT, TVSERIES_ID) VALUES (?, ?, ?)";
	private String selectALLSQL = "SELECT * FROM SEASON";
	private String deleteAllSQL = "DELETE FROM SEASON";
	private String findIdSQL = "SELECT ID FROM DIRECTOR WHERE NAME = ?";
	
	public void add(DBConnect connection, Season season, String tvSeriesName) throws SQLException {
		TvSeriesAction tvseriesaction = new TvSeriesAction();
		PreparedStatement addActorQuery = connection.getConnection().prepareStatement(addSQL);
		addActorQuery.setInt(1, season.getSeasonNumber());
		addActorQuery.setInt(2, season.getYearOfRelease());
		addActorQuery.setInt(3, tvseriesaction.getId(connection, tvSeriesName));
		addActorQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			Season season = new Season();
			season. setId(selectAllQuery.getInt("ID"));
			season.setSeasonNumber(selectAllQuery.getInt("SEASON_NUMBER"));
			season.setYearOfRelease(selectAllQuery.getInt("YEAR_OF_RELEASE"));
			seasons.add(season);
		}
		selectAllQuery.beforeFirst();
		return seasons;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteSeasonsQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteSeasonsQuery.executeUpdate();
	}
	
	public int getId(DBConnect connection, int seasonNbr) throws SQLException {
		int result = 0;
		PreparedStatement findIdQuery = connection.getConnection().prepareStatement(findIdSQL);
		findIdQuery.setInt(1, seasonNbr);
		ResultSet rs = findIdQuery.executeQuery();
		if(rs.next()) {
			result = rs.getInt(1);
		}
		return result;
	}

}