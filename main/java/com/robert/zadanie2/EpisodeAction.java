package com.robert.zadanie2;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Episode;

public class EpisodeAction {
	
	List <Episode> episodes = new ArrayList();
	private String addSQL = "INSERT INTO EPISODE (NAME, RELEASE_DATE, EPISODE_NUMBER, DURATION, SEASON_ID) VALUES (?, ?, ?, ?, ?)";
	private String selectALLSQL = "SELECT * FROM EPISODE";
	private String deleteAllSQL = "DELETE FROM EPISODE";
	
	public void add(DBConnect connection, Episode episode, int seasonNbr) throws SQLException {
		SeasonAction season = new SeasonAction();
		int id = season.getId(connection, seasonNbr);
		PreparedStatement addEpisodeQuery = connection.getConnection().prepareStatement(addSQL);
		addEpisodeQuery.setString(1, episode.getName());
		addEpisodeQuery.setDate(2, Date.valueOf(episode.getReleaseDate()));
		addEpisodeQuery.setInt(3, episode.getEpisodeNumber());
		addEpisodeQuery.setInt(4, episode.getDuration());
		addEpisodeQuery.setInt(5, id);
		addEpisodeQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			Episode episode = new Episode();
			episode. setId(selectAllQuery.getInt("ID"));
			episode.setName(selectAllQuery.getString("NAME"));
			episode.setReleaseDate(selectAllQuery.getDate("RELEASE_DATE").toLocalDate());
			episodes.add(episode);
		}
		selectAllQuery.beforeFirst();
		return episodes;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteEpisodesQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteEpisodesQuery.executeUpdate();
	}

}