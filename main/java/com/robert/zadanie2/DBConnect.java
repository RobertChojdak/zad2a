package com.robert.zadanie2;

import java.sql.*;
import java.util.*;

public class DBConnect {
	
	private String url = "jdbc:hsqldb:hsql://localhost/testdb";
	private String usr = "SA";
	private String pwd = "";
	private Connection conn;
	private Statement statement;
	private ResultSet result;
	private DatabaseMetaData dbmt;
	private String tabToCreate;
	
	private String TVSERIES = "CREATE TABLE TVSERIES(ID INT IDENTITY, NAME VARCHAR(40), DIRECTOR_ID INT)";
    private String SEASON = "CREATE TABLE SEASON(ID INT IDENTITY, SEASON_NUMBER INT, YEAR_OF_RELEASE INT, TVSERIES_ID INT)";
    private String EPISODE = "CREATE TABLE EPISODE(ID INT IDENTITY, NAME VARCHAR(30), RELEASE_DATE DATE, EPISODE_NUMBER INT, DURATION INT, SEASON_ID INT)";
    private String DIRECTOR = "CREATE TABLE DIRECTOR(ID INT IDENTITY, NAME VARCHAR(20), DATE_OF_BIRTH DATE, BIOGRAPHY VARCHAR(400))";
    private String ACTOR = "CREATE TABLE ACTOR(ID INT IDENTITY, NAME VARCHAR(20), DATE_OF_BIRTH DATE, BIOGRAPHY VARCHAR(400))";
    private String TAMAPPING = "CREATE TABLE TAMAPPING(ID INT IDENTITY, TVSERIES_ID INT, ACTOR_ID INT)";
   
    private boolean isTvSeries = false;
    private boolean isSeason = false;
    private boolean isEpisode = false;
    private boolean isDirector = false;
    private boolean isActor = false;
    private boolean isMapping = false;
	
	
	DBConnect() {
		try {
			 //System.out.println("Registering driver");

            //Class.forName("org.hsqldb.jdbc.JDBCDriver");
         
            System.out.println("Connecting to DataBase");
            conn = DriverManager.getConnection(url, usr, pwd);

            if(conn != null) System.out.println("You are connected to DataBase");
            if(conn == null) System.out.println("You are not connected to DataBase");
                    
            statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);                        
                        
            dbmt =  conn.getMetaData();

            result = dbmt.getTables(null, null, null, null);

            tableCheck(result);
            if(!isTvSeries) statement.executeUpdate(TVSERIES);
            if(!isSeason) statement.executeUpdate(SEASON);
            if(!isEpisode) statement.executeUpdate(EPISODE);
            if(!isDirector) statement.executeUpdate(DIRECTOR);
            if(!isActor) statement.executeUpdate(ACTOR);
            if(!isMapping) statement.execute(TAMAPPING);
            
            conn.close();
            
		}
		catch(SQLException se) {
			se.getMessage();
		}
		//catch (ClassNotFoundException ex) {
		//	System.out.println("Error - unable to load driver class");
		//	System.exit(1);
		//}
		finally {
			try {
				if(conn != null) {
					conn.close();
				}
			}
			catch(SQLException se) {
				se.getMessage();
			}
		}
	}
	
	private void tableCheck(ResultSet result) throws SQLException {
        while(result.next()) {
        	tabToCreate = result.getString("TABLE_NAME").toUpperCase();
            //System.out.println(tabToCreate);
            if(tabToCreate.equals("TVSERIES"))        isTvSeries = true;
            else if(tabToCreate.equals("SEASON")) isSeason = true;
            else if(tabToCreate.equals("EPISODE")) isEpisode = true;
            else if(tabToCreate.equals("DIRECTOR")) isDirector = true;
            else if(tabToCreate.equals("ACTOR")) isActor = true;
            else if(tabToCreate.equals("TAMAPPING")) isMapping = true;
        }
        result.beforeFirst();
	}
	
	public Connection getConnection() throws SQLException {
		return conn = DriverManager.getConnection(url, usr, pwd);
	}
}
