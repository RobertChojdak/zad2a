package com.robert.zadanie2;

import java.util.*;
import model.Director;
import java.sql.*;
import java.sql.Date;

public class DirectorAction {
	
	List <Director> directors = new ArrayList();
	private String addSQL = "INSERT INTO DIRECTOR (NAME, DATE_OF_BIRTH, BIOGRAPHY) VALUES (?, ?, ?)";
	private String selectALLSQL = "SELECT * FROM DIRECTOR";
	private String deleteAllSQL = "DELETE FROM DIRECTOR";
	private String findIdSQL = "SELECT ID FROM DIRECTOR WHERE NAME = ?";
		
	public void add(DBConnect connection, Director director) throws SQLException {
		PreparedStatement addQuery = connection.getConnection().prepareStatement(addSQL);
		addQuery.setString(1, director.getName());
		addQuery.setDate(2, Date.valueOf(director.getDateOfBirth()));
		addQuery.setString(3, director.getBiography());
		addQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			Director director = new Director();
			director. setId(selectAllQuery.getInt("ID"));
			director.setName(selectAllQuery.getString("NAME"));
			director.setDateOfBirth(selectAllQuery.getDate("DATE_OF_BIRTH").toLocalDate());
			director.setBiography(selectAllQuery.getString("BIOGRAPHY"));
			directors.add(director);
		}
		selectAllQuery.beforeFirst();
		return directors;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteAllQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteAllQuery.executeUpdate();
	}
	
	public int getId(DBConnect connection, String directorName) throws SQLException {
		int result = 0;
		PreparedStatement findIdQuery = connection.getConnection().prepareStatement(findIdSQL);
		findIdQuery.setString(1, directorName);
		ResultSet rs = findIdQuery.executeQuery();
		if(rs.next()) {
			result = rs.getInt(1);
		}
		return result;
	}

}